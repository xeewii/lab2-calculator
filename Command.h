#pragma once
#include <stack>
#include <string>
#include <iostream>
#include <map>
#include "Exception.h"

using namespace std;

class Command
{
public:
	virtual void Do(stack<string>&, map<string, double>&, string) = 0;
	~Command();
};

class Pop: public Command {
public:
	void Do(stack<string>&, map<string, double>&, string);
};

class Push : public Command {
public:
	void Do(stack<string>&, map<string, double>&, string);
};

class Plus : public Command {
public:
	void Do(stack<string>&, map<string, double>&, string);
};

class Minus : public Command {
public:
	void Do(stack<string>&, map<string, double>&, string);
};

class Multiplication : public Command {
public:
	void Do(stack<string>&, map<string, double>&, string);
};

class Devision : public Command {
public:
	void Do(stack<string>&, map<string, double>&, string);
};

class Sqrt : public Command {
public:
	void Do(stack<string>&, map<string, double>&, string);
};

class Define : public Command {
public:
	void Do(stack<string>&, map<string, double>&, string);
};

class Print : public Command {
public:
	void Do(stack<string>&, map<string, double>&, string);
};