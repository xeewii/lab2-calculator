#pragma once
#include <fstream>
#include <stack>
#include <string>
#include <iostream>
#include <map>
#include "Factory.h"
#include "Exception.h"

using namespace std;

class Calculator
{
	fstream in_file;
	stack<string> stack;
	map<string, double> variables;
	map<string, Factory*> factories;

public:
	Calculator(string file);

	void Count();

	const double Resultat();

	void Create_facories();

	~Calculator();
};