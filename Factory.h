#pragma once
#include "Command.h"

class Factory
{
public:
	virtual Command* Create_command()=0;
	~Factory() {};
};

class Factory_pop : public Factory {
public:
	Command* Create_command() { return new Pop; }
};

class Factory_push : public Factory {
public:
	Command* Create_command() { return new Push; }
};

class Factory_plus : public Factory {
public:
	Command* Create_command() { return new Plus; }
};

class Factory_minus : public Factory {
public:
	Command* Create_command() { return new Minus; }
};

class Factory_multiplication : public Factory {
public:
	Command* Create_command() { return new Multiplication; }
};

class Factory_devision : public Factory {
public:
	Command* Create_command() { return new Devision; }
};

class Factory_sqrt : public Factory {
public:
	Command* Create_command() { return new Sqrt; }
};

class Factory_print : public Factory {
public:
	Command* Create_command() { return new Print; }
};

class Factory_define : public Factory {
public:
	Command* Create_command() { return new Define; }
};