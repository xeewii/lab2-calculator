#pragma once
#include <exception>

using namespace std;


class Arithmetic_exception : public exception {};

class File_exception : public exception {};

class User_exception : public exception {};

class Error_opening_the_file : public File_exception
{
public:
	const char * what() const throw () {
		return "Error opening the file!";
	}
};

class Devision_by_zero : public Arithmetic_exception {
public:
	const char * what() const throw () {
		return "Devision by zero!";
	}
};

class Sqrt_of_a_negative_number : public Arithmetic_exception {
public:
	const char * what() const throw () {
		return "Sqrt of a negative number!";
	}
};

class Is_NaN : public Arithmetic_exception {
public:
	const char * what() const throw () {
		return "Resultat is NaN!";
	}
};

class Empty_stack :public User_exception {
public:
	const char * what() const throw () {
		return "Empty stack!";
	}
};

class Bad_command : public User_exception {
public:
	const char * what() const throw() {
		return "Bad command!";
	}
};
