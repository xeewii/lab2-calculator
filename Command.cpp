#include "Command.h"

void Read_the_number(stack<string>& stack, map<string, double>& variables, double& number) {
	if (stack.empty()) throw Empty_stack();
	string arg = stack.top();
	if (variables[arg] != 0) {
		number = variables[arg];
	}
	else number = atof(arg.c_str());
	stack.pop();
}

Command::~Command()
{
}

void Pop::Do(stack<string>& stack, map<string, double>& variables, string arg) {
	if (stack.empty()) throw Empty_stack();
	stack.pop();
}

void Push::Do(stack<string>& stack, map<string, double>& variables, string arg) {
	stack.push(arg);
}

void Plus::Do(stack<string>& stack, map<string, double>& variables, string arg) {
	double a1, a2;
	Read_the_number(stack, variables, a1);
	Read_the_number(stack, variables, a2);
	stack.push(to_string(a1 + a2));
}

void Minus::Do(stack<string>& stack, map<string, double>& variables, string arg) {
	double a1, a2;
	Read_the_number(stack, variables, a1);
	Read_the_number(stack, variables, a2);
	stack.push(to_string(a2 - a1));
}

void Multiplication::Do(stack<string>& stack, map<string, double>& variables, string arg) {
	double a1, a2;
	Read_the_number(stack, variables, a1);
	Read_the_number(stack, variables, a2);
	stack.push(to_string(a1*a2));
}

void Devision::Do(stack<string>& stack, map<string, double>& variables, string arg) {
	double a1, a2;
	Read_the_number(stack, variables, a1);
	Read_the_number(stack, variables, a2);
	if (a1 == 0)
		if (a2 == 0) throw Is_NaN();
		else throw Devision_by_zero();
	stack.push(to_string(a2 / a1));
}


void Sqrt::Do(stack<string>& stack, map<string, double>& variables, string arg) {
	double a;
	Read_the_number(stack, variables, a);
	if (a < 0) throw Sqrt_of_a_negative_number();
	stack.push(to_string(sqrt(a)));
}

void Print::Do(stack<string>& stack, map<string, double>& variables, string arg) {
	if (stack.empty()) throw Empty_stack();
	cout << stack.top() << endl;
}

void Define::Do(stack<string>& stack, map<string, double>& variables, string arg) {
	char space = arg.find(' ');
	string s = arg.substr(0, space); //переменная
	arg = arg.substr(space + 1, arg.size()); //значение
	variables[s] = atof(arg.c_str());
}
