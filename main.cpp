#include <iostream>
#include <string>
#include <fstream>
#include "Calculator.h"

using namespace std;

int main(int argc, char* argv[]) {
	string file;
	if (argc > 1) file = argv[1];
	else file = "cin";
	Calculator calculator(file);
	calculator.Count();

	system("pause");
	return 0;
}