#include "Calculator.h"

Calculator::Calculator(string file)
{
	if (file != "cin") {
		try {
			in_file.open(file, std::ios_base::in);
			cin.rdbuf(in_file.rdbuf());
			if (!in_file.is_open()) throw Error_opening_the_file();
		}
		catch (exception& err) {
			cout << err.what() << endl;
			exit(0);
		}
	}

}

const double Calculator::Resultat() {
	if (variables[stack.top()]) return variables[stack.top()];
	else return atof(stack.top().c_str());
}

void Calculator::Create_facories() {
	Factory_pop* factory_pop = new Factory_pop;
	factories["pop"] = factory_pop;
	Factory_push* factory_push = new Factory_push;
	factories["push"] = factory_push;
	Factory_define* factory_define = new Factory_define;
	factories["define"] = factory_define;
	Factory_plus* factory_plus = new Factory_plus;
	factories["+"] = factory_plus;
	Factory_minus* factory_minus = new Factory_minus;
	factories["-"] = factory_minus;
	Factory_multiplication* factory_multiplication = new Factory_multiplication;
	factories["*"] = factory_multiplication;
	Factory_devision* factory_devision = new Factory_devision;
	factories["/"] = factory_devision;
	Factory_sqrt* factory_sqrt = new Factory_sqrt;
	factories["sqrt"] = factory_sqrt;
	Factory_print* factory_print = new Factory_print;
	factories["print"] = factory_print;
}

void Read_command(string& c, string& arg) {
	char space = c.find(' ');
	arg = c;
	c = c.substr(0, space);
	arg = arg.substr(space + 1, c.size());
}

void Calculator::Count() {
	string command, arg;
	Create_facories();

	while (getline(cin, command)) {
		try {
			Read_command(command, arg);
			if (factories[command]) {
				factories[command]->Create_command()->Do(stack, variables, arg);
			}
			else throw Bad_command();
		}
		catch (exception& err) {
			cout << err.what() << endl;
			throw; // для тестов
		}
	}
}

Calculator::~Calculator()
{
	in_file.close();
}
